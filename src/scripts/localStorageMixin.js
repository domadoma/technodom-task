export default {
  methods: {
    changeStorage: function (storageName, value) {
      localStorage.setItem(storageName, JSON.stringify(value))
    },
    clearStorage: function () {
      localStorage.clear()
    },
    removeItem: function (storageName) {
      localStorage.removeItem(storageName)
    }
  }
}

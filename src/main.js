// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// Importing main styling
import './assets/scss/main.scss'

// Import libraries
import Donut from 'vue-css-donut-chart'
import 'vue-css-donut-chart/dist/vcdonut.css'

// Import components
import NAVIGATION from './pages/components/navigation'
import RETURN from './pages/components/return'
Vue.component('navigation', NAVIGATION)
Vue.component('return', RETURN)

Vue.use(Donut)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
